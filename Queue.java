
public class Queue<T> {
	
	private Node<T> first;
	
	private Node<T> last;
	
	public Queue() {
		
		first = null;
		
		last = null;
		
	}
	
	public boolean isEmpty() {
		
		return first == null;
		
	}
	
	public void enqueue(T item) {
		
		Node<T> oldLast = last;
		
		last.setDatum(item);
		
		last.setNext(null);
		
		if(isEmpty()) first = last;
		
		else oldLast.setNext(last);
		
	}
	
	public T dequeue() {
		
		T item = first.getDatum();
		
		first = first.getNext();
		
		if(isEmpty()) last = null;
		
		return item;
		
	}
}

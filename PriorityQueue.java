
public class PriorityQueue<T extends Comparable<T>> {
	
	private Node<T> first;
	
	public PriorityQueue() {
		
		first = null;
		
	}
	
	public boolean isEmpty() {
		
		return first == null;
		
	}
	
	public void insert(T item) {
		
		Node<T> temp = new Node<T>(item, null);
		
		Node<T> previous = null;
		
		Node<T> current = first;
		
		while(current != null && current.getDatum().compareTo(item) > 0) {
			
			previous = current;
			
			current = current.getNext();
			
		}
		
		if(previous == null) first = temp;
		
		else {
			
			previous.setNext(temp);
			
			temp.setNext(current);
			
		}
	}
	
	public void clear() {
		
		while(!isEmpty()) {
			
			poll();
			
		}
	}
	
	public T peek() {
		
		return first.getDatum();
	}
	
	public T poll() {
		
		Node<T> temp = first;
		
		first = first.getNext();
		
		return temp.getDatum();
	}
}

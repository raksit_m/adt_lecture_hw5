import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

public class Heap<T extends Comparable<T>> {
	
	private Vector<T> heap;
	
	public Heap() {
		
		heap = new Vector<T>();
		
	}
	
	public void insert(T item) {
		
		heap.add(item);
		
		rotateUp(heap.size() - 1);
		
	}
	
	public T extract() {
		
		if(heap.size() == 0) return null;
		
		T temp = heap.elementAt(0);
		
		heap.setElementAt(heap.elementAt(heap.size() - 1), 0);
		
		heap.setSize(heap.size() - 1);
		
		rotateDown(0);
		
		return temp;
		
	}
	
	public void rotateUp(int index) {
		
		if(index == 0) return;
		
		int parent = (index - 1) / 2;
		
		if(heap.elementAt(parent).compareTo(heap.elementAt(index)) <= 0) {
			
			return;
		}
		
		Collections.swap(heap, index, parent);
		
		rotateUp(parent);
		
	}
	
	public void rotateDown(int index) {
		
		int child = 2*(index + 1);
		
		if(child >= heap.size() || heap.elementAt(child - 1).compareTo(heap.elementAt(child)) < 0) {
			
			child -= 1;
			
		}
		
		if(child >= heap.size()) return;
		
		if(heap.elementAt(index).compareTo(heap.elementAt(child)) <= 0) {
			
			return;
			
		}
		
		Collections.swap(heap, index, child);
		
		rotateDown(child);
		
	}
	
	public void heapSort(ArrayList<Comparable<T>> list) {
		
		java.util.PriorityQueue<Comparable<T>> pq = new java.util.PriorityQueue<Comparable<T>>(list);
		
		for(int i = 0; i < list.size(); i++) list.set(i, pq.poll());
		
	}
	
	public void displayHeap() {
		
		for(int i = 0; i < heap.size(); i++) System.out.print(heap.elementAt(i) + " ");
	}
	
	public static void main(String[] args) {
		
		ArrayList<Comparable<Integer>> list = new ArrayList<Comparable<Integer>>();
		
		list.add(11);
		
		list.add(3);
		
		list.add(5);
		
		list.add(6);
		
		list.add(4);
		
		list.add(23);
		
		list.add(9);
		
		System.out.println(list);
		
		java.util.PriorityQueue<Comparable<Integer>> pq = new java.util.PriorityQueue<Comparable<Integer>>(list);
		
		for(int i = 0; i < list.size(); i++) list.set(i, pq.poll());
		
		System.out.println(list);
		
//		Heap<Integer> heap = new Heap<Integer>();
//		
//		heap.insert(100);
//		
//		heap.insert(80);
//		
//		heap.insert(90);
//		
//		heap.insert(60);
//		
//		heap.insert(70);
//		
//		heap.insert(50);
//		
//		heap.insert(85);
//		
//		heap.insert(13);
//		
//		heap.displayHeap();
//		
//		System.out.println();
//		
//		System.out.println(heap.extract());
//		
//		heap.displayHeap();
	}
}